webpackHotUpdate_N_E("pages/job/[jobId]/offer/[offerId]",{

/***/ "./pages/job/[jobId]/offer/[offerId]/index.tsx":
/*!*****************************************************!*\
  !*** ./pages/job/[jobId]/offer/[offerId]/index.tsx ***!
  \*****************************************************/
/*! exports provided: JobOrderOfferDetails, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JobOrderOfferDetails", function() { return JobOrderOfferDetails; });
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ "../../node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ "../../node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "../../node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-query */ "../../node_modules/react-query/es/index.js");
/* harmony import */ var _templateOffer__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../templateOffer */ "./pages/job/[jobId]/offer/templateOffer.tsx");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! styled-components */ "../../node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var _wayfindinghq_ecopros_kit__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wayfindinghq/ecopros-kit */ "../../node_modules/@wayfindinghq/ecopros-kit/index.js");
/* harmony import */ var lib_api__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! lib/api */ "./lib/api.ts");
/* harmony import */ var _components_common_job_offer_details__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @components/common/job-offer-details */ "./components/common/job-offer-details/index.tsx");


var _jsxFileName = "C:\\Users\\azochniak\\Desktop\\ecop\\trading-platform\\packages\\frontend\\pages\\job\\[jobId]\\offer\\[offerId]\\index.tsx",
    _s = $RefreshSig$();









var Card = styled_components__WEBPACK_IMPORTED_MODULE_5__["default"].div.withConfig({
  displayName: "offerId__Card",
  componentId: "zjacve-0"
})(["background:white;border-radius:20px;padding:20px;"]);
_c = Card;
function JobOrderOfferDetails() {
  _s();

  var _jobOffers$data, _jobOffers$data2;

  var router = Object(next_router__WEBPACK_IMPORTED_MODULE_1__["useRouter"])();
  var jobId = Object(react__WEBPACK_IMPORTED_MODULE_2__["useMemo"])(function () {
    return parseInt(router.query.jobId);
  }, [router.asPath]);
  var offerId = Object(react__WEBPACK_IMPORTED_MODULE_2__["useMemo"])(function () {
    return parseInt(router.query.offerId);
  }, [router.asPath]);
  var jobOffers = Object(react_query__WEBPACK_IMPORTED_MODULE_3__["useQuery"])(['job', jobId, 'offers'
  /*, offerId*/
  ], function () {
    return Object(lib_api__WEBPACK_IMPORTED_MODULE_7__["getJobOffers"])(jobId).then(function (r) {
      return r.data;
    });
  }, {
    initialData: [],
    onSuccess: function onSuccess(v) {
      debugger;
    }
  });
  console.log('info', ['jobid', jobId, 'offerid', offerId], jobOffers === null || jobOffers === void 0 ? void 0 : jobOffers.data);
  var offersDet = jobOffers === null || jobOffers === void 0 ? void 0 : (_jobOffers$data = jobOffers.data) === null || _jobOffers$data === void 0 ? void 0 : _jobOffers$data.find(function (e) {
    return e.id == offerId;
  });
  return /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_templateOffer__WEBPACK_IMPORTED_MODULE_4__["default"], {
    job: jobOffers === null || jobOffers === void 0 ? void 0 : (_jobOffers$data2 = jobOffers.data) === null || _jobOffers$data2 === void 0 ? void 0 : _jobOffers$data2.find(function (e) {
      return e.id == jobId;
    }),
    offer: offersDet,
    selectedValue: "offerDetails",
    children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
      children: [/*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(Card, {
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_wayfindinghq_ecopros_kit__WEBPACK_IMPORTED_MODULE_6__["ProductSpecification"], {
          title: "Szczeg\xF3\u0142y oferty",
          items: offersDet ? [{
            label: 'ID',
            value: "#".concat(offersDet.id)
          }, {
            label: 'Cena',
            value: offersDet.priceNet
          }, {
            label: 'Realizacja do',
            value: new Date(offersDet.executionUntil).toLocaleDateString('pl')
          }, {
            label: 'Telefon',
            value: offersDet.phoneNumber
          }, {
            label: 'Informacje',
            value: offersDet.additionalInfo
          }] : []
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 46,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 45,
        columnNumber: 9
      }, this), /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])("div", {
        style: {
          marginTop: '600px'
        },
        children: /*#__PURE__*/Object(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__["jsxDEV"])(_components_common_job_offer_details__WEBPACK_IMPORTED_MODULE_8__["default"], {}, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 73,
          columnNumber: 11
        }, this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 9
      }, this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 41,
      columnNumber: 7
    }, this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 37,
    columnNumber: 5
  }, this);
}

_s(JobOrderOfferDetails, "BALXCrgV84V6bPZVXdH68AejN1I=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_1__["useRouter"], react_query__WEBPACK_IMPORTED_MODULE_3__["useQuery"]];
});

_c2 = JobOrderOfferDetails;
/* harmony default export */ __webpack_exports__["default"] = (JobOrderOfferDetails);

var _c, _c2;

$RefreshReg$(_c, "Card");
$RefreshReg$(_c2, "JobOrderOfferDetails");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.i);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../../../../node_modules/next/dist/compiled/webpack/harmony-module.js */ "../../node_modules/next/dist/compiled/webpack/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvam9iL1tqb2JJZF0vb2ZmZXIvW29mZmVySWRdL2luZGV4LnRzeCJdLCJuYW1lcyI6WyJDYXJkIiwic3R5bGVkIiwiZGl2IiwiSm9iT3JkZXJPZmZlckRldGFpbHMiLCJyb3V0ZXIiLCJ1c2VSb3V0ZXIiLCJqb2JJZCIsInVzZU1lbW8iLCJwYXJzZUludCIsInF1ZXJ5IiwiYXNQYXRoIiwib2ZmZXJJZCIsImpvYk9mZmVycyIsInVzZVF1ZXJ5IiwiZ2V0Sm9iT2ZmZXJzIiwidGhlbiIsInIiLCJkYXRhIiwiaW5pdGlhbERhdGEiLCJvblN1Y2Nlc3MiLCJ2IiwiY29uc29sZSIsImxvZyIsIm9mZmVyc0RldCIsImZpbmQiLCJlIiwiaWQiLCJsYWJlbCIsInZhbHVlIiwicHJpY2VOZXQiLCJEYXRlIiwiZXhlY3V0aW9uVW50aWwiLCJ0b0xvY2FsZURhdGVTdHJpbmciLCJwaG9uZU51bWJlciIsImFkZGl0aW9uYWxJbmZvIiwibWFyZ2luVG9wIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBSUE7QUFDQTtBQUVBO0FBQ0E7QUFFQSxJQUFNQSxJQUFJLEdBQUdDLHlEQUFNLENBQUNDLEdBQVY7QUFBQTtBQUFBO0FBQUEseURBQVY7S0FBTUYsSTtBQU1DLFNBQVNHLG9CQUFULEdBQThDO0FBQUE7O0FBQUE7O0FBQ25ELE1BQU1DLE1BQU0sR0FBR0MsNkRBQVMsRUFBeEI7QUFDQSxNQUFNQyxLQUFLLEdBQUdDLHFEQUFPLENBQUMsWUFBTTtBQUFDLFdBQU9DLFFBQVEsQ0FBQ0osTUFBTSxDQUFDSyxLQUFQLENBQWFILEtBQWQsQ0FBZjtBQUE4QyxHQUF0RCxFQUF3RCxDQUFDRixNQUFNLENBQUNNLE1BQVIsQ0FBeEQsQ0FBckI7QUFDQSxNQUFNQyxPQUFPLEdBQUdKLHFEQUFPLENBQUMsWUFBTTtBQUFDLFdBQU9DLFFBQVEsQ0FBQ0osTUFBTSxDQUFDSyxLQUFQLENBQWFFLE9BQWQsQ0FBZjtBQUFnRCxHQUF4RCxFQUEwRCxDQUFDUCxNQUFNLENBQUNNLE1BQVIsQ0FBMUQsQ0FBdkI7QUFFQSxNQUFNRSxTQUFTLEdBQUdDLDREQUFRLENBQUMsQ0FBQyxLQUFELEVBQVFQLEtBQVIsRUFBZTtBQUFRO0FBQXZCLEdBQUQsRUFBd0M7QUFBQSxXQUFNUSw0REFBWSxDQUFDUixLQUFELENBQVosQ0FBb0JTLElBQXBCLENBQXlCLFVBQUFDLENBQUM7QUFBQSxhQUFJQSxDQUFDLENBQUNDLElBQU47QUFBQSxLQUExQixDQUFOO0FBQUEsR0FBeEMsRUFBcUY7QUFDN0dDLGVBQVcsRUFBRSxFQURnRztBQUU3R0MsYUFGNkcscUJBRW5HQyxDQUZtRyxFQUVoRztBQUNYO0FBQ0Q7QUFKNEcsR0FBckYsQ0FBMUI7QUFNQUMsU0FBTyxDQUFDQyxHQUFSLENBQVksTUFBWixFQUFvQixDQUFDLE9BQUQsRUFBVWhCLEtBQVYsRUFBaUIsU0FBakIsRUFBNEJLLE9BQTVCLENBQXBCLEVBQTBEQyxTQUExRCxhQUEwREEsU0FBMUQsdUJBQTBEQSxTQUFTLENBQUVLLElBQXJFO0FBRUEsTUFBSU0sU0FBUyxHQUFJWCxTQUFKLGFBQUlBLFNBQUosMENBQUlBLFNBQVMsQ0FBRUssSUFBZixvREFBRyxnQkFBcURPLElBQXJELENBQTBELFVBQUFDLENBQUM7QUFBQSxXQUFJQSxDQUFDLENBQUNDLEVBQUYsSUFBUWYsT0FBWjtBQUFBLEdBQTNELENBQWhCO0FBRUEsc0JBQ0UscUVBQUMsc0RBQUQ7QUFDRSxPQUFHLEVBQUVDLFNBQUYsYUFBRUEsU0FBRiwyQ0FBRUEsU0FBUyxDQUFFSyxJQUFiLHFEQUFFLGlCQUFpQk8sSUFBakIsQ0FBc0IsVUFBQUMsQ0FBQztBQUFBLGFBQUlBLENBQUMsQ0FBQ0MsRUFBRixJQUFRcEIsS0FBWjtBQUFBLEtBQXZCLENBRFA7QUFFRSxTQUFLLEVBQUVpQixTQUZUO0FBR0UsaUJBQWEsRUFBQyxjQUhoQjtBQUFBLDJCQUlFO0FBQUEsOEJBSUUscUVBQUMsSUFBRDtBQUFBLCtCQUNFLHFFQUFDLDhFQUFEO0FBQ0UsZUFBSyxFQUFDLDBCQURSO0FBRUUsZUFBSyxFQUFFQSxTQUFTLEdBQUcsQ0FDakI7QUFDRUksaUJBQUssRUFBRSxJQURUO0FBRUVDLGlCQUFLLGFBQU1MLFNBQVMsQ0FBQ0csRUFBaEI7QUFGUCxXQURpQixFQUtqQjtBQUNFQyxpQkFBSyxFQUFFLE1BRFQ7QUFFRUMsaUJBQUssRUFBRUwsU0FBUyxDQUFDTTtBQUZuQixXQUxpQixFQVNqQjtBQUNFRixpQkFBSyxFQUFFLGVBRFQ7QUFFRUMsaUJBQUssRUFBRSxJQUFJRSxJQUFKLENBQVNQLFNBQVMsQ0FBQ1EsY0FBbkIsRUFBbUNDLGtCQUFuQyxDQUFzRCxJQUF0RDtBQUZULFdBVGlCLEVBYWpCO0FBQ0VMLGlCQUFLLEVBQUUsU0FEVDtBQUVFQyxpQkFBSyxFQUFFTCxTQUFTLENBQUNVO0FBRm5CLFdBYmlCLEVBaUJqQjtBQUNFTixpQkFBSyxFQUFFLFlBRFQ7QUFFRUMsaUJBQUssRUFBRUwsU0FBUyxDQUFDVztBQUZuQixXQWpCaUIsQ0FBSCxHQXFCWjtBQXZCTjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxjQUpGLGVBK0JFO0FBQUssYUFBSyxFQUFFO0FBQUNDLG1CQUFTLEVBQUU7QUFBWixTQUFaO0FBQUEsK0JBQ0UscUVBQUMsNEVBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsY0EvQkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBSkY7QUFBQTtBQUFBO0FBQUE7QUFBQSxVQURGO0FBMkNEOztHQTFEZWhDLG9CO1VBQ0NFLHFELEVBSUdRLG9EOzs7TUFMSlYsb0I7QUE0RERBLG1GQUFmIiwiZmlsZSI6InN0YXRpYy93ZWJwYWNrL3BhZ2VzL2pvYi9bam9iSWRdL29mZmVyL1tvZmZlcklkXS5lNTYyY2QxMWJhM2JjYTRjM2EzOC5ob3QtdXBkYXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSm9iT3JkZXJNb2RhbCB9IGZyb20gJ0Bjb21wb25lbnRzL2NvbW1vbi9qb2Itb3JkZXItbW9kYWwnXHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gJ25leHQvcm91dGVyJ1xyXG5pbXBvcnQgeyBSZWFjdEVsZW1lbnQsIHVzZU1lbW8gfSBmcm9tICdyZWFjdCdcclxuaW1wb3J0IHsgdXNlUXVlcnkgfSBmcm9tICdyZWFjdC1xdWVyeSdcclxuaW1wb3J0IEpvYk9mZmVyVGVtcGxhdGUgZnJvbSAnLi4vdGVtcGxhdGVPZmZlcidcclxuXHJcbmltcG9ydCB7IENyZWF0ZUpvYk9mZmVyRHRvIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vYmFja2VuZC9zcmMvY29tcG9uZW50cy9qb2Itb2ZmZXIvam9iLW9mZmVyLmR0bydcclxuaW1wb3J0IHsgRm9ybVByb3ZpZGVyLCB1c2VGb3JtIH0gZnJvbSAncmVhY3QtaG9vay1mb3JtJ1xyXG5pbXBvcnQgc3R5bGVkIGZyb20gJ3N0eWxlZC1jb21wb25lbnRzJ1xyXG5pbXBvcnQgeyBQcm9kdWN0U3BlY2lmaWNhdGlvbiB9IGZyb20gJ0B3YXlmaW5kaW5naHEvZWNvcHJvcy1raXQnXHJcbmltcG9ydCB7IFNwZWNpZmljYXRpb25JdGVtIH0gZnJvbSAnQHdheWZpbmRpbmdocS9lY29wcm9zLWtpdC9kaXN0L2NvbXBvbmVudHMvbW9sZWN1bGVzL1Byb2R1Y3QvUHJvZHVjdFNwZWNpZmljYXRpb24vUHJvZHVjdFNwZWNpZmljYXRpb24udHlwZXMnXHJcbmltcG9ydCB7IGdldEpvYk9mZmVycyB9IGZyb20gJ2xpYi9hcGknXHJcbmltcG9ydCBKb2JPZmZlckRldGFpbHMgZnJvbSAnQGNvbXBvbmVudHMvY29tbW9uL2pvYi1vZmZlci1kZXRhaWxzJ1xyXG5cclxuY29uc3QgQ2FyZCA9IHN0eWxlZC5kaXZgXHJcbiAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG5gXHJcblxyXG5leHBvcnQgZnVuY3Rpb24gSm9iT3JkZXJPZmZlckRldGFpbHMoKTogUmVhY3RFbGVtZW50IHtcclxuICBjb25zdCByb3V0ZXIgPSB1c2VSb3V0ZXIoKVxyXG4gIGNvbnN0IGpvYklkID0gdXNlTWVtbygoKSA9PiB7cmV0dXJuIHBhcnNlSW50KHJvdXRlci5xdWVyeS5qb2JJZCBhcyBzdHJpbmcpfSwgW3JvdXRlci5hc1BhdGhdKVxyXG4gIGNvbnN0IG9mZmVySWQgPSB1c2VNZW1vKCgpID0+IHtyZXR1cm4gcGFyc2VJbnQocm91dGVyLnF1ZXJ5Lm9mZmVySWQgYXMgc3RyaW5nKX0sIFtyb3V0ZXIuYXNQYXRoXSlcclxuICBcclxuICBjb25zdCBqb2JPZmZlcnMgPSB1c2VRdWVyeShbJ2pvYicsIGpvYklkLCAnb2ZmZXJzJy8qLCBvZmZlcklkKi9dLCAoKSA9PiBnZXRKb2JPZmZlcnMoam9iSWQpLnRoZW4ociA9PiByLmRhdGEpLCB7XHJcbiAgICBpbml0aWFsRGF0YTogW10sXHJcbiAgICBvblN1Y2Nlc3Modikge1xyXG4gICAgICBkZWJ1Z2dlclxyXG4gICAgfVxyXG4gIH0pXHJcbiAgY29uc29sZS5sb2coJ2luZm8nLCBbJ2pvYmlkJywgam9iSWQsICdvZmZlcmlkJywgb2ZmZXJJZF0sIGpvYk9mZmVycz8uZGF0YSlcclxuXHJcbiAgbGV0IG9mZmVyc0RldCA9IChqb2JPZmZlcnM/LmRhdGEgYXMgdW5rbm93biBhcyBDcmVhdGVKb2JPZmZlckR0b1tdKT8uZmluZChlID0+IGUuaWQgPT0gb2ZmZXJJZClcclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxKb2JPZmZlclRlbXBsYXRlIFxyXG4gICAgICBqb2I9e2pvYk9mZmVycz8uZGF0YT8uZmluZChlID0+IGUuaWQgPT0gam9iSWQpfVxyXG4gICAgICBvZmZlcj17b2ZmZXJzRGV0fVxyXG4gICAgICBzZWxlY3RlZFZhbHVlPSdvZmZlckRldGFpbHMnPlxyXG4gICAgICA8ZGl2PlxyXG4gICAgICAgIHsvKiA8aDE+U3pjemVnw7PFgnkgb2ZlcnR5ITwvaDE+ICovfVxyXG4gICAgICAgIHsvKiA8c21hbGw+e29mZmVyc0RldD8uY3JlYXRlZEF0fTwvc21hbGw+ICovfVxyXG4gICAgICAgIHsvKiA8cHJlPntKU09OLnN0cmluZ2lmeShqb2JPZmZlcnMsIG51bGwsIDIpfTwvcHJlPiAqL31cclxuICAgICAgICA8Q2FyZD5cclxuICAgICAgICAgIDxQcm9kdWN0U3BlY2lmaWNhdGlvblxyXG4gICAgICAgICAgICB0aXRsZT1cIlN6Y3plZ8OzxYJ5IG9mZXJ0eVwiXHJcbiAgICAgICAgICAgIGl0ZW1zPXtvZmZlcnNEZXQgPyBbXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdJRCcsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogYCMke29mZmVyc0RldC5pZH1gLFxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdDZW5hJyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiBvZmZlcnNEZXQucHJpY2VOZXQsXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ1JlYWxpemFjamEgZG8nLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IG5ldyBEYXRlKG9mZmVyc0RldC5leGVjdXRpb25VbnRpbCkudG9Mb2NhbGVEYXRlU3RyaW5nKCdwbCcpLFxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgbGFiZWw6ICdUZWxlZm9uJyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiBvZmZlcnNEZXQucGhvbmVOdW1iZXIsXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBsYWJlbDogJ0luZm9ybWFjamUnLFxyXG4gICAgICAgICAgICAgICAgdmFsdWU6IG9mZmVyc0RldC5hZGRpdGlvbmFsSW5mbyxcclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBdIDogW119XHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9DYXJkPlxyXG4gICAgICAgIDxkaXYgc3R5bGU9e3ttYXJnaW5Ub3A6ICc2MDBweCd9fT5cclxuICAgICAgICAgIDxKb2JPZmZlckRldGFpbHMvPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIHsvKiA8SW5zdGFsbGF0aW9uRm9ybS8+ICovfVxyXG4gICAgICA8L2Rpdj5cclxuICAgIDwvSm9iT2ZmZXJUZW1wbGF0ZT5cclxuICApXHJcbn1cclxuXHJcbmV4cG9ydCBkZWZhdWx0IEpvYk9yZGVyT2ZmZXJEZXRhaWxzXHJcbiJdLCJzb3VyY2VSb290IjoiIn0=